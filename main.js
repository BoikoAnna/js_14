//Реалізувати перемикання вкладок (таби) на чистому Javascript.

//Технічні вимоги:
//У папці tabs лежить розмітка для вкладок. 
//Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки.
// При цьому решта тексту повинна бути прихована. 
//У коментарях зазначено, який текст має відображатися для якої вкладки.
//Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
//Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися
// та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, 
//через такі правки не переставала працювати.



//Завдання
//Реалізувати можливість зміни колірної теми користувача.
//Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

//Технічні вимоги:
//Взяти будь-яке готове домашнє завдання з HTML/CSS.
//Додати на макеті кнопку "Змінити тему".
//При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
//Вибрана тема повинна зберігатися після перезавантаження сторінки


let tab=function(){
let tabNav = document.querySelectorAll('.tabs-nav_item'),
tabContent=document.querySelectorAll('.tab'),
tabName;

tabNav.forEach (item => {
item.addEventListener('click', selectTabNav)
});

function selectTabNav(){
tabNav.forEach (item => {
item.classList.remove('is-active')
});
this.classList.add('is-active');
tabName=this.getAttribute('data-tab-name');
selectTabContent(tabName);
};

function selectTabContent (tabName) {
tabContent.forEach(item =>{
item.classList.contains(tabName) ? item.classList.add('is-active') : item.classList.remove('is-active');
console.log(tabName);
})
}

};

tab();



if (localStorage.getItem('theme')){
document.body.classList.add('dark');
}

const toggleButton = document.querySelector('#toggle-mode');
toggleButton.addEventListener('click', (e) => {
document.body.classList.toggle('dark');
 
if (document.body.classList.contains('dark')){
localStorage.setItem('theme', true)
} else {
localStorage.removeItem('theme') 
}
});